﻿using System;
using System.Collections.Generic;
using TP3_CLIENT.ViewModels;
using Xamarin.Forms;

namespace TP3_CLIENT.Views
{
    public partial class CompteSearchPage : ContentPage
    {
        public CompteSearchPage()
        {
            InitializeComponent();
            BindingContext = ((ViewModelLocator)Application.Current.Resources["Locator"]).compteSearchViewModel;
        }
    }
}
