﻿using System;
using System.Collections.Generic;
using TP3_CLIENT.ViewModels;
using Xamarin.Forms;

namespace TP3_CLIENT.Views
{
    public partial class CompteCreatePage : ContentPage
    {
        public CompteCreatePage()
        {
            InitializeComponent();
            BindingContext = ((ViewModelLocator)Application.Current.Resources["Locator"]).compteCreateViewModel;
        }
    }
}
