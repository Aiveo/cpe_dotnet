﻿using System;
using System.Collections.Generic;
using TP3_CLIENT.ViewModels;
using TP3_CLIENT.Views;
using Xamarin.Forms;

namespace TP3_CLIENT
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(CompteSearchPage), typeof(CompteSearchPage));
            Routing.RegisterRoute(nameof(CompteCreatePage), typeof(CompteCreatePage));
        }

        private async void OnMenuItemClicked(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync("//LoginPage");
        }
    }
}
