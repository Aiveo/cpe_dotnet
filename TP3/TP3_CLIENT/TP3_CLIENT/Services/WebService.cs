﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TP3_CLIENT.Models;

namespace TP3_CLIENT.Services
{
    public class WebService
    {
        private static readonly HttpClient client = new HttpClient();
        private static readonly Lazy<WebService> lazy = new Lazy<WebService>(() => new WebService());

        public static WebService Instance { get { return lazy.Value; } }

        private WebService()
        {
            client.BaseAddress = new Uri("http://vincentcouturier.net/api/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<Compte> GetCompteByEmailAsync(string email)
        {
            Compte compte = null;

            try
            {
                HttpResponseMessage response = await client.GetAsync("compte?email=" + email);
                response.EnsureSuccessStatusCode();
                string responseBodyJSON = await response.Content.ReadAsStringAsync();
                compte = JsonConvert.DeserializeObject<Compte>(responseBodyJSON);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }

            return compte;
        }

        public async Task<Compte> PostCompte(Compte compteToPost)
        {
            Compte compte = null;

            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(compteToPost), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync("compte", content);
                response.EnsureSuccessStatusCode();
                string responseBodyJSON = await response.Content.ReadAsStringAsync();
                compte = JsonConvert.DeserializeObject<Compte>(responseBodyJSON);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }

            return compte;
        }
    }
}