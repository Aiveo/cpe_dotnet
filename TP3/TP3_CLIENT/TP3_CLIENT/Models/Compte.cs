﻿using System;
namespace TP3_CLIENT.Models
{
    public class Compte
    {
        
        public int CompteId { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string TelPortable { get; set; }
        public string Mel { get; set; }
        public string Pwd { get; set; }
        public string Rue { get; set; }
        public string CodePostal { get; set; }
        public string Ville { get; set; }
        public string Pays { get; set; }
        public Nullable<float> Latitude { get; set; }
        public Nullable<float> Longitude { get; set; }
        public DateTime DateCreation { get; set; }


        public Compte()
        {
        }
    }
}
