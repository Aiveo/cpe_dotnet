﻿using System;
namespace TP3_CLIENT.Models
{
    public class Film
    {
        public int FilmId { get; set; }
        public string Titre { get; set; }
        public string Synopsis { get; set; }
        public DateTime DateParution { get; set; }
        public decimal Duree { get; set; }
        public string Genre { get; set; }
        public string UrlPhoto { get; set; }

        public Film()
        {
        }
    }
}
