﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using TP3_CLIENT.Models;
using TP3_CLIENT.Services;

namespace TP3_CLIENT.ViewModels
{
    public class CompteCreateViewModel : ViewModelBase
    {
        private Compte _compteCreate;
        public Compte CompteCreate
        {
            get { return this._compteCreate; }
            set
            {
                this._compteCreate = value;
                RaisePropertyChanged();
            }
        }

        public ICommand BtnSearch { get; private set; }
        public ICommand BtnAjout { get; private set; }

        public CompteCreateViewModel()
        {
            CompteCreate = new Compte();

            BtnSearch = new RelayCommand(GetCoordinate);
            BtnAjout = new RelayCommand(CreateCompte);
        }

        private async void CreateCompte()
        {
            var compte = await WebService.Instance.PostCompte(CompteCreate);
        }

        private async void GetCoordinate()
        {
        }
    }
}
