﻿using System;
using GalaSoft.MvvmLight.Ioc;

namespace TP3_CLIENT.ViewModels
{
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            SimpleIoc.Default.Register<CompteSearchViewModel>();
            SimpleIoc.Default.Register<CompteCreateViewModel>();
        }

        public CompteSearchViewModel compteSearchViewModel => SimpleIoc.Default.GetInstance<CompteSearchViewModel>();
        public CompteCreateViewModel compteCreateViewModel => SimpleIoc.Default.GetInstance<CompteCreateViewModel>();

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}
