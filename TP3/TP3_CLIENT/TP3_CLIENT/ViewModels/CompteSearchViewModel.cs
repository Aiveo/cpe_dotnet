﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using TP3_CLIENT.Models;
using TP3_CLIENT.Services;

namespace TP3_CLIENT.ViewModels
{
    public class CompteSearchViewModel : ViewModelBase
    {
        public string MelSearch { get; set; }
        private Compte _compteSearch;
        public Compte CompteSearch {
            get { return this._compteSearch; }
            set {
                this._compteSearch = value;
                RaisePropertyChanged();
            }
        }

        public ICommand BtnSearch { get; private set; }
        public ICommand BtnClear { get; private set; }

        public CompteSearchViewModel()
        {
            //Set une email par default au demarage de l'application
            MelSearch = "paul.durand@gmail.com";

            BtnSearch = new RelayCommand(getCompteByEmail);
            BtnClear = new RelayCommand(ClearFields);
        }

        public async void getCompteByEmail()
        {
            CompteSearch = await WebService.Instance.GetCompteByEmailAsync(MelSearch);
        }

        public void ClearFields()
        {
            CompteSearch = null;
        }
    }
}
