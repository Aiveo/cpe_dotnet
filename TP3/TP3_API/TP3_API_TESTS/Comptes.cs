﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TP3_API.Controllers;
using TP3_API.Models.DataManager;
using TP3_API.Models.EntityFramework;
using TP3_API.Models.Repository;

namespace TP3_API_TESTS
{
    [TestClass]
    public class Comptes
    {
        private DataBaseContext _context = null;
        private CompteController _controller = null;
        private IDataRepository<Compte> _dataRepository;

        [TestInitialize]
        public void InitialisationDesTests()
        {
            var builder = new DbContextOptionsBuilder<DataBaseContext>().UseNpgsql("Server=localhost;port=5432;Database=TP3_API_DB; uid=postgres; password=postgres;");
            _context = new DataBaseContext(builder.Options);
            _dataRepository = new CompteManager(_context);
            _controller = new CompteController(_dataRepository);

        }

        [TestMethod]
        public async System.Threading.Tasks.Task GetCompteByIdAsync()
        {
            const int id = 1;
            var compteGet = _controller.GetCompteById(id).Result.Value;
            Assert.IsInstanceOfType(compteGet, typeof(Compte), "Pas un compte");

            var compteDB = await _context.Comptes.FindAsync(id);
            Assert.AreEqual(compteDB, compteGet, "Comptes pas identiques");
        }

        [TestMethod]
        public async System.Threading.Tasks.Task GetCompteByEmailAsync()
        {
            const string email = "paul.durand@gmail.com";
            var compteGet = _controller.GetCompteByEmail(email).Result.Value;
            Assert.IsInstanceOfType(compteGet, typeof(Compte), "Pas un compte");

            var compteDB = await _context.Comptes.FirstOrDefaultAsync(c => c.Mel == email);
            Assert.AreEqual(compteDB, compteGet, "Comptes pas identiques");
        }

        [TestMethod]
        public void PostCompte_ModelValidated_Creation_OK()
        {
            // Arrange
            Random rnd = new Random();
            int chiffre = rnd.Next(1, 1000000000);

            Compte compteAtester = new Compte()
            {
                Nom = "MACHIN",
                Prenom = "Luc",
                TelPortable = "0606070809",
                Mel = "machin" + chiffre + "@gmail.com",
                Pwd = "Toto1234!",
                Rue = "Chemin de Bellevue",
                CodePostal = "74940",
                Ville = "Annecy-le-Vieux",
                Pays = "France",
                Latitude = null,
                Longitude = null
            };

            // Act
            var result = _controller.PostCompte(compteAtester).Result;
            var result2 = _controller.GetCompteByEmail(compteAtester.Mel);
            var actionResult = result2.Result as ActionResult<Compte>;

            // Assert
            Assert.IsInstanceOfType(actionResult.Value, typeof(Compte), "Pas un compte");
            Compte compteRecupere = _context.Comptes.Where(c => c.Mel == compteAtester.Mel).FirstOrDefault();

            // On ne connait pas l'ID du compte envoyé car numéro automatique.
            // Du coup, on récupère l'ID de celui récupéré et on compare ensuite les 2 comptes
            compteAtester.CompteId = compteRecupere.CompteId;
            Assert.AreEqual(compteRecupere, compteAtester, "Comptes pas identiques");
        }

        [TestMethod]
        [ExpectedException(typeof(System.AggregateException))]
        public void PostCompte_ModelValidated_Creation_NOK()
        {
            //Arrange
            var compte = _context.Comptes.First(c => c.CompteId == 1);

            //Act
            var result = _controller.PostCompte(compte).Result;
        }

        [TestCleanup]
        public void NettoyageDesTests()
        {
            _context = null;
            _controller = null;
        }
    }
}