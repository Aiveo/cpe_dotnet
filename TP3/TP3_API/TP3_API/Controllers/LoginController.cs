﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using TP3_API.Models.EntityFramework;
using TP3_API.Models.Repository;

namespace TP3_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IDataRepository<Compte> _dataRepository;

        public LoginController(IConfiguration config, IDataRepository<Compte> dataRepository)
        {
            _config = config;
            _dataRepository = dataRepository;
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Login(Compte login)
        {
            IActionResult response = Unauthorized();

            Compte compte = AuthenticateUser(login);

            if (compte != null)
            {
                var tokenString = GenerateJwtToken(compte);

                response = Ok(new {
                    token = tokenString,
                    compteDetails = compte,
                });
            }

            return response;
        }

        private Compte AuthenticateUser(Compte compte)
        {
            Compte response = null;

            var compteBDD = _dataRepository.GetByString(compte.Mel).Result.Value;
            if ((compte.Mel.ToLower() == compteBDD.Mel.ToLower()) && (compte.Pwd == compteBDD.Pwd))
            {
                response = compteBDD;
            }
            return response;
        }

        private string GenerateJwtToken(Compte compteInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:SecretKey"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, compteInfo.Mel),
                new Claim("Nom", compteInfo.Nom),
                new Claim("Prenom", compteInfo.Prenom),
                new Claim("UserRole", compteInfo.UserRole),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };
            var token = new JwtSecurityToken(
                issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Audience"],
                claims: claims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}