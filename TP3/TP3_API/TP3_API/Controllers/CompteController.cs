using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TP3_API.Model;
using TP3_API.Models.DataManager;
using TP3_API.Models.EntityFramework;
using TP3_API.Models.Repository;

namespace TP3_API.Controllers
{
    [Route("api/[controller]")]
    //[Route("api/[controller]/[action]")]
    [ApiController]
    public class CompteController : ControllerBase
    {
        private readonly IDataRepository<Compte> _dataRepository;

        public CompteController(IDataRepository<Compte> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        // GET: api/Compte
        [HttpGet]
        [Authorize(Policy = Policies.Admin)]
        public async Task<ActionResult<IEnumerable<Compte>>> GetComptes()
        {
            return await _dataRepository.GetAll();
        }

        // GET: api/Compte/GetCompteById/5
        [HttpGet("GetCompteById/{id}")]
        //[HttpGet("{id}")]
        //[ActionName("GetCompteById")]
        public async Task<ActionResult<Compte>> GetCompteById(int id)
        {
            var compte = await _dataRepository.GetById(id);

            if (compte == null)
            {
                return NotFound("Id compte inconnu");
            }

            return compte;
        }

        // GET: api/Compte/GetCompteByEmail/lmorandet@cpe.fr
        [HttpGet("GetCompteByEmail/{email}")]
        //[HttpGet("{email}")]
        //[ActionName("GetCompteById")]
        public async Task<ActionResult<Compte>> GetCompteByEmail(string email)
        {
            var compte = await _dataRepository.GetByString(email);

            if (compte == null)
            {
                return NotFound("Email compte inconnu");
            }

            return compte;
        }

        // PUT: api/Compte/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCompte(int id, Compte compte)
        {
            if (id != compte.CompteId)
            {
                return BadRequest();
            }

            var compteToUpdate = await _dataRepository.GetById(id);

            if (compteToUpdate == null)
            {
                return NotFound();
            }

            await _dataRepository.Update(compteToUpdate.Value, compte);

            return NoContent();
        }

        // POST: api/Compte
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Compte>> PostCompte(Compte compte)
        {
            await _dataRepository.Add(compte);

            return CreatedAtAction("GetCompteById", new { id = compte.CompteId }, compte);
        }

        //DELETE: api/Compte/5
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteCompte(int id)
        //{
        //    var compte = _compteManager.GetById(id);

        //    if (compte == null)
        //    {
        //        return NotFound("Id compte inconnu");
        //    }


        //    _dataRepository.Delete(compte.Value);

        //    return NoContent();
        //}

        // PATCH: api/Compte/5
        [HttpPatch("{id}")]
        public async Task<ActionResult<Compte>> PatchAsync(int id, [FromBody] JsonPatchDocument<Compte> patchEntity)
        {
            var compte = await _dataRepository.GetById(id);
            var compteToUpdate = await _dataRepository.GetById(id);

            if (compte == null)
            {
                return NotFound("Id compte inconnu");
            }

            patchEntity.ApplyTo(compte.Value, ModelState);

            await _dataRepository.Update(compteToUpdate.Value, compte.Value);

            return compte;
        }
    }
}
