﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TP3_API.Migrations
{
    public partial class AddUserRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CPT_ROLE",
                schema: "public",
                table: "T_E_COMPTE_CPT",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CPT_ROLE",
                schema: "public",
                table: "T_E_COMPTE_CPT");
        }
    }
}
