﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TP3_API.Models.EntityFramework
{
    [Table("T_J_FAVORI_FAV")]
    public partial class Favori
    {
        [Key]
        [Column("CPT_ID")]
        public int CompteId { get; set; }

        [Key]
        [Column("FLM_ID")]
        public int FilmId { get; set; }

        [Column("FAV_COMMENTAIRE")]
        [StringLength(500, ErrorMessage = "La longueur du commentaire doit etre inferieure a 500 caracteres")]
        public string Commentaire { get; set; }

        [ForeignKey(nameof(CompteId))]
        [InverseProperty("FavorisFilm")]
        public virtual Film FilmFavori { get; set; }

        [ForeignKey(nameof(FilmId))]
        [InverseProperty("FavorisCompte")]
        public virtual Compte CompteFavori { get; set; }

        public Favori()
        {
        }
    }
}
