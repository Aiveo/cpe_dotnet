﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace TP3_API.Models.EntityFramework
{
    public partial class DataBaseContext : DbContext
    {
        public virtual DbSet<Compte> Comptes { get; set; }
        public virtual DbSet<Favori> Favoris { get; set; }
        public virtual DbSet<Film> Films { get; set; }

        public static readonly ILoggerFactory MyLoggerFactory = LoggerFactory.Create(builder => builder.AddConsole());

        public DataBaseContext()
        {
        }

        public DataBaseContext(DbContextOptions<DataBaseContext> options) : base(options)
        {
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {
        //        optionsBuilder.UseLoggerFactory(MyLoggerFactory)
        //            .EnableSensitiveDataLogging();
        //    }
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "fr_FR.utf8");

            modelBuilder.HasDefaultSchema("public");

            modelBuilder.Entity<Compte>(entity =>
            {
                entity.Property(c => c.DateCreation)
                    .HasDefaultValueSql("current_date"); ;

                entity.Property(c => c.Pays)
                    .HasDefaultValue("France");

                entity.HasIndex(c => c.Mel)
                    .IsUnique();
            });

            modelBuilder.Entity<Favori>(entity =>
            {
                entity.HasKey(F => new { F.CompteId, F.FilmId })
                    .HasName("PK_FAV");

                entity.HasOne(F => F.FilmFavori)
                    .WithMany(f => f.FavorisFilm)
                    .HasForeignKey(F => F.FilmId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_FAV_FLM");

                entity.HasOne(F => F.CompteFavori)
                    .WithMany(c => c.FavorisCompte)
                    .HasForeignKey(F => F.CompteId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_FAV_CPT");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
