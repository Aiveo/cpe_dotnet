﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TP3_API.Models.EntityFramework
{
    [Table("T_E_COMPTE_CPT")]
    public partial class Compte
    {
        [Key]
        [Column("CPT_ID")]
        public int CompteId { get; set; }

        [Required]
        [Column("CPT_NOM")]
        [StringLength(50, ErrorMessage = "La longueur du nom doit etre inferieure a 50 caracteres")]
        public string Nom { get; set; }

        [Required]
        [Column("CPT_PRENOM")]
        [StringLength(50, ErrorMessage = "La longueur du prénom doit etre inferieure a 50 caracteres")]
        public string Prenom { get; set; }

        [Column("CPT_TELPORTABLE", TypeName="char(10)")]
        [RegularExpression(@"^0[0-9]{9}$", ErrorMessage = "Le numéro de téléphone est composé de 10 Chiffres")]
        public string TelPortable { get; set; }

        [Required]
        [Column("CPT_MEL")]
        [EmailAddress(ErrorMessage = "La valeure rentrée n'est pas une adresse mail")]
        [StringLength(100, MinimumLength = 6, ErrorMessage = "La longueur d’un email doit être comprise entre 6 et 100 caractères")]
        public string Mel { get; set; }

        [Column("CPT_PWD")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,10}$",
            ErrorMessage = "Le mot de passe doit faire entre 6 et 10 caractères et doit comporté au minimum un chiffre, une minuscule et une majuscule")]
        [StringLength(64)]
        public string Pwd { get; set; }

        [Required]
        [Column("CPT_ROLE")]
        public string UserRole { get; set; }

        [Required]
        [Column("CPT_RUE")]
        [StringLength(200, ErrorMessage = "La longueur de la rue doit etre inferieure a 200 caracteres")]
        public string Rue { get; set; }

        [Required]
        [Column("CPT_CP", TypeName = "char(5)")]
        [RegularExpression(@"^\d{5}$", ErrorMessage = "Le code postal est composé de 5 chiffres")]
        public string CodePostal { get; set; }

        [Required]
        [Column("CPT_VILLE")]
        [StringLength(50, ErrorMessage = "La longueur de la ville doit etre inferieure a 50 caracteres")]
        public string Ville { get; set; }

        [Required]
        [Column("CPT_PAYS")]
        [StringLength(50, ErrorMessage = "La longueur du pays doit etre inferieure a 50 caracteres")]
        public string Pays { get; set; }

        [Column("CPT_LATITUDE")]
        public Nullable<float> Latitude { get; set; }

        [Column("CPT_LONGITUDE")]
        public Nullable<float> Longitude { get; set; }

        [Required]
        [Column("CPT_DATECREATION", TypeName = "date")]
        //[RegularExpression(@"^\d{2]\/\d{2]\/\d{4]$", ErrorMessage = "La date doit etre au format jj/mm/aaaa")]
        public DateTime DateCreation { get; set; }

        [InverseProperty(nameof(Favori.CompteFavori))]
        public virtual ICollection<Favori> FavorisCompte { get; set; }

        public Compte()
        {
        }
    }
}
