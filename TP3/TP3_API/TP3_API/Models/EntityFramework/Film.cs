﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TP3_API.Models.EntityFramework
{
    [Table("T_E_FILM_FLM")]
    public partial class Film
    {
        [Key]
        [Column("FLM_ID")]
        public int FilmId { get; set; }

        [Required]
        [Column("FLM_TITRE")]
        [StringLength(100, ErrorMessage = "La longueur du titre doit etre inferieure a 100 caracteres")]
        public string Titre { get; set; }

        [Column("FLM_SYNOPSIS")]
        [StringLength(500, ErrorMessage = "La longueur du synopsis doit etre inferieure a 500 caracteres")]
        public string Synopsis { get; set; }

        [Required]
        [Column("FLM_DATEPARUTION", TypeName = "date")]
        //[RegularExpression(@"^\d{2]\/\d{2]\/\d{4]$", ErrorMessage = "La date doit etre au format jj/mm/aaaa")]
        public DateTime DateParution { get; set; }

        [Required]
        [Column("FLM_DUREE")]
        public decimal Duree { get; set; }

        [Required]
        [Column("FLM_GENRE")]
        [StringLength(30, ErrorMessage = "La longueur du genre doit etre inferieure a 30 caracteres")]
        public string Genre { get; set; }

        [Column("FLM_URLPHOTO")]
        [Url(ErrorMessage = "La valeure n'est pas une URL")]
        [StringLength(200, ErrorMessage = "La longueur de l'URL doit etre inferieure a 200 caracteres")]
        public string UrlPhoto { get; set; }

        [InverseProperty(nameof(Favori.FilmFavori))]
        public virtual ICollection<Favori> FavorisFilm { get; set; }

        public Film()
        {
        }
    }
}
