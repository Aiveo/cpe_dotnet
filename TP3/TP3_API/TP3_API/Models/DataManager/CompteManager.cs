﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TP3_API.Models.EntityFramework;
using TP3_API.Models.Repository;

namespace TP3_API.Models.DataManager
{
    public class CompteManager : IDataRepository<Compte>
    {
        readonly DataBaseContext _dataBaseContext;
        public CompteManager(DataBaseContext context)
        {
            _dataBaseContext = context;
        }
        public async Task<ActionResult<IEnumerable<Compte>>> GetAll()
        {
            return await _dataBaseContext.Comptes.ToListAsync();
        }

        public async Task<ActionResult<Compte>> GetById(int id)
        {
            return await _dataBaseContext.Comptes.FirstOrDefaultAsync(e => e.CompteId == id);
        }

        public async Task<ActionResult<Compte>> GetByString(string mail)
        {
            return await _dataBaseContext.Comptes.FirstOrDefaultAsync(e => e.Mel.ToUpper() == mail.ToUpper());
        }

        public async Task Add(Compte entity)
        {
            _dataBaseContext.Comptes.Add(entity);
            await _dataBaseContext.SaveChangesAsync();
        }

        public async Task Update(Compte compte, Compte entity)
        {
            _dataBaseContext.Entry(compte).State = EntityState.Modified;

            compte.CompteId = entity.CompteId;
            compte.Nom = entity.Nom;
            compte.Prenom = entity.Prenom;
            compte.Mel = entity.Mel;
            compte.Rue = entity.Rue;
            compte.CodePostal = entity.CodePostal;
            compte.Ville = entity.Ville;
            compte.Pays = entity.Pays;
            compte.Latitude = entity.Latitude;
            compte.Longitude = entity.Longitude;
            compte.Pwd = entity.Pwd;
            compte.TelPortable = entity.TelPortable;
            compte.FavorisCompte = entity.FavorisCompte;

            await _dataBaseContext.SaveChangesAsync();
        }
        public async Task Delete(Compte compte)
        {
            _dataBaseContext.Comptes.Remove(compte);
            await _dataBaseContext.SaveChangesAsync();
        }
    }
}
