﻿using System;
namespace TP2Console.Models.EntityFramework
{
    public partial class Utilisateur
    {
        public override string ToString()
        {
            return Id + " : " + Login;
        }
    }
}
