﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TP2Console.Models.EntityFramework;

namespace TP2Console
{
    class Program
    {
        static void Main(string[] args)
        {
            //Exo2Q1();
            //Exo2Q1Bis();
            //Exo2Q2();
            //Exo2Q3();
            //Exo2Q4();
            //Exo2Q5();
            //Exo2Q6();
            //Exo2Q7();
            //Exo2Q8();
            //Exo2Q9();

            //Exo3Q1();
            //Exo3Q2();
            //Exo3Q3();
            //Exo3Q4();
            Exo3Q5();

            Console.ReadKey();
        }

        public static void Exo2Q1()
        {
            var ctx = new FilmsDBContext();
            foreach (var film in ctx.Films)
            {
                Console.WriteLine(film.ToString());
            }
        }

        public static void Exo2Q1Bis()
        {
            var ctx = new FilmsDBContext();
            //Pour que cela marche, il faut que la requête envoie les mêmes noms de colonnes que les classes c#.
            var films = ctx.Films.FromSqlRaw("SELECT * FROM film");
            foreach (var film in films)
            {
                Console.WriteLine(film.ToString());
            }
        }

        public static void Exo2Q2()
        {
            var ctx = new FilmsDBContext();
            foreach (var utilisateur in ctx.Utilisateurs)
            {
                Console.WriteLine(utilisateur.Email);
            }
        }

        public static void Exo2Q3()
        {
            var ctx = new FilmsDBContext();
            var utilisateurs = ctx.Utilisateurs.OrderBy(u => u.Login);
            foreach (var utilisateur in utilisateurs)
            {
                Console.WriteLine(utilisateur.Login);
            }
        }

        public static void Exo2Q4()
        {
            var ctx = new FilmsDBContext();
            var categorie = ctx.Categories.First(c => c.Nom == "Action");
            ctx.Entry(categorie).Collection(c => c.Films).Load();
            foreach (var film in categorie.Films)
            {
                Console.WriteLine(film.ToString());
            }
        }

        public static void Exo2Q5()
        {
            var ctx = new FilmsDBContext();
            var categorie = ctx.Categories.Count();
            Console.WriteLine("Nombre de categorie : " + categorie);
        }

        public static void Exo2Q6()
        {
            var ctx = new FilmsDBContext();
            var avis = ctx.Avis.First(a => a.Note == ctx.Avis.Min(a => a.Note));
            Console.WriteLine("Note la plus basse : " + avis.Note);
            Console.WriteLine("Avis : " + avis.Avis);
        }

        public static void Exo2Q7()
        {
            var ctx = new FilmsDBContext();
            var films = ctx.Films.Count(f => f.Nom.StartsWith("Le"));
            Console.WriteLine("Nombre de films commencant par 'Le' : " + films);
        }

        public static void Exo2Q8()
        {
            var ctx = new FilmsDBContext();
            var avis = ctx.Avis.Where(a => a.FilmNavigation.Nom.Equals("Pulp fiction")).Average(n => n.Note);
            Console.WriteLine("Moyenne notes de Pulp Fiction : " + avis);
        }

        public static void Exo2Q9()
        {
            var ctx = new FilmsDBContext();
            // Deux requetes
            var avis = ctx.Avis.First(a => a.Note == ctx.Avis.Max(a => a.Note));
            var user = ctx.Utilisateurs.First(u => u.Id == avis.Utilisateur);
            Console.WriteLine("Utilisateur avec la note la plus haute : " + user);

            //Une requete
            var utilisateur = ctx.Utilisateurs.First(u => ctx.Avis.Where(a => a.Utilisateur == u.Id).Max(a => a.Note) == ctx.Avis.Max(a => a.Note));
            Console.WriteLine("Utilisateur avec la note la plus haute : " + utilisateur);
        }

        public static void Exo3Q1()
        {
            var ctx = new FilmsDBContext();

            //Création et initialisation de l’objet
            var moi = new Utilisateur();
            moi.Login = "test";
            moi.Email = "test@example.com";
            moi.Pwd = "P@ssW0rd";

            //Ajout au contexte.
            ctx.Utilisateurs.Add(moi);

            //Sauvegarde du contexte
            ctx.SaveChanges();
        }

        public static void Exo3Q2()
        {
            var ctx = new FilmsDBContext();
            var film = ctx.Films.First(f => f.Nom.Equals("L'armee des douze singes"));

            //Ajout d'une description
            film.Description = "Ceci est une decription Test pour le film";

            //Changement de categorie
            var categorie = ctx.Categories.First(c => c.Nom.Equals("Drame"));
            film.Categorie = categorie.Id;

            ctx.SaveChanges();
        }

        public static void Exo3Q3()
        {
            var ctx = new FilmsDBContext();
            var film = ctx.Films.First(f => f.Nom.Equals("L'armee des douze singes"));

            //Suppression du film
            ctx.Films.Remove(film);

            //Suppression des avis
            ctx.Entry(film).Collection(f => f.Avis).Load();
            foreach (var avis in film.Avis)
            {
                ctx.Avis.Remove(avis);
            }

            ctx.SaveChanges();
        }

        public static void Exo3Q4()
        {
            var ctx = new FilmsDBContext();
            var film = ctx.Films.First(f => f.Nom.Contains("Titanic"));

            //Rajout d'un avis
            var user = ctx.Utilisateurs.First(u => u.Login.Equals("test"));
            var avis = new Avi();

            avis.Film = film.Id;
            avis.Utilisateur = user.Id;
            avis.Note = 0.5m;
            avis.Avis = "Ceci est un avis test";
            ctx.Avis.Add(avis);

            ctx.SaveChanges();
        }

        public static void Exo3Q5()
        {
            var ctx = new FilmsDBContext();

            var categorie = ctx.Categories.First(c => c.Nom.Equals("Drame"));

            //Creation de la liste de films a ajouter
            var newFilms = new List<Film>() {
                new Film() {
                    Nom = "Film Trop Bien",
                    Description = "C'est l'histoire d'un film trop bien",
                    Categorie = categorie.Id
                },
                new Film() {
                    Nom = "Film Trop Bien : Le Retour",
                    Description = "Apres moulte peripeties, c'est le retour d'un film trop bien",
                    Categorie = categorie.Id
                }
            };

            //Ajout des films
            ctx.Films.AddRange(newFilms);
            ctx.SaveChanges();
        }
    }
}