﻿using System;
using System.Collections.Generic;
using Convertisseur.ViewModels;
using Xamarin.Forms;

namespace Convertisseur.Views
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = ((ViewModelLocator)Application.Current.Resources["Locator"]).Main;
        }
    }
}
