﻿using System;
using GalaSoft.MvvmLight.Ioc;

namespace Convertisseur.ViewModels
{
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            SimpleIoc.Default.Register<MainViewModel>();
        }
        public MainViewModel Main => SimpleIoc.Default.GetInstance<MainViewModel>();
        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}
