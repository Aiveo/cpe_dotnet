﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Convertisseur.Models;
using Convertisseur.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Convertisseur.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private ObservableCollection<Devise> _listesDevises;
        private string _montantEuros;
        private Devise _selectedItem;
        private string _montantDevise;

        public ICommand BtnSetConversion { get; private set; }

        public ObservableCollection<Devise> ListeDevises
        {
            get { return _listesDevises; }
            set
            {
                _listesDevises = value;
                RaisePropertyChanged();// Pour notifier de la modification de ses données }
            }
        }

        public string MontantEuros
        {
            get { return _montantEuros; }
            set
            {
                _montantEuros = value;
                RaisePropertyChanged();
            }
        }

        public Devise SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                RaisePropertyChanged();
            }
        }

        public string MontantDevise
        {
            get { return _montantDevise; }
            set
            {
                _montantDevise = value;
                RaisePropertyChanged();
            }
        }

        public MainViewModel()
        {
            ActionGetData();
            BtnSetConversion = new RelayCommand(ActionSetConversion);
        }

        private async void ActionGetData()
        {
            var result = await WebService.Instance.GetAllDevisesAsync();
            this.ListeDevises = new ObservableCollection<Devise>(result);
        }

        private void ActionSetConversion()
        {
            double result = int.Parse(MontantEuros) * SelectedItem.taux;
            MontantDevise = result.ToString();
        }
    }
}
