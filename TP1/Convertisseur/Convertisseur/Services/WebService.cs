﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Convertisseur.Models;
using Newtonsoft.Json;

namespace Convertisseur.Services
{
    public class WebService
    {
        private static readonly HttpClient client = new HttpClient();
        private static readonly Lazy<WebService> lazy = new Lazy<WebService>(() => new WebService());

        public static WebService Instance { get { return lazy.Value; } }

        private WebService()
        {
            client.BaseAddress = new Uri("http://vincentcouturier.fr/api/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<Devise>> GetAllDevisesAsync()
        {
            List<Devise> deviseList = new List<Devise>();

            try
            {
                HttpResponseMessage response = await client.GetAsync("devise");
                response.EnsureSuccessStatusCode();
                string responseBodyJSON = await response.Content.ReadAsStringAsync();
                deviseList = JsonConvert.DeserializeObject<List<Devise>>(responseBodyJSON);
                return deviseList;
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return deviseList;
            }
        }
    }
}
