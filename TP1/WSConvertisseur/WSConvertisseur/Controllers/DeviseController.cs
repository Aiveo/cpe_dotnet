using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WSConvertisseur.Models;

namespace WSConvertisseur.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeviseController : ControllerBase
    {
        public List<Devise> DevisesList;

        /// <summary>
        /// Get all currency.
        /// </summary>
        /// <returns>Http response</returns>
        /// <response code="200">Return all currency</response>

        // GET: api/Devise
        [HttpGet]
        public IEnumerable<Devise> GetAll()
        {
            return DevisesList;
        }

        /// <summary>
        /// Get a single currency.
        /// </summary>
        /// <returns>Http response</returns>
        /// <param name="id">The id of the currency</param>
        /// <response code="200">When the currency id is found</response>
        /// <response code="404">When the currency id is not found</response>

        // GET: api/Devise/5
        [HttpGet("{id}", Name = "GetDevise")]
        public IActionResult GetById(int id)
        {
            //Devise devise =
            //    (from d in DevisesList
            //     where d.Id == id
            //     select d).FirstOrDefault();

            Devise devise = DevisesList.FirstOrDefault((d) => d.Id == id);

            if (devise == null)
            {
                return NotFound();
            }
            return Ok(devise);
        }

        /// <summary>
        /// Post a currency.
        /// </summary>
        /// <returns>Http response</returns>
        /// <param name="id">The id of the currency</param>
        /// <response code="200">When the currency id is found</response>
        /// <response code="404">When the currency id is not found</response>

        // POST: api/Devise
        [HttpPost]
        public IActionResult Post([FromBody] Devise devise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            DevisesList.Add(devise);
            return CreatedAtRoute("GetDevise", new { id = devise.Id }, devise);
        }

        // PUT: api/Devise/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Devise devise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != devise.Id)
            {
                return BadRequest();
            }
            int index = DevisesList.FindIndex((d) => d.Id == id);

            if (index < 0)
            {
                return NotFound();
            }
            DevisesList[index] = devise;
            return NoContent();
        }

        // DELETE: api/Devise/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Devise devise = DevisesList.FirstOrDefault((d) => d.Id == id);

            if (devise == null)
            {
                return NotFound();
            }

            DevisesList.Remove(devise);
            return Ok(devise);
        }

        public DeviseController()
        {
            DevisesList = new List<Devise>();
            DevisesList.Add(new Devise(1, "Dollar", 1.08));
            DevisesList.Add(new Devise(2, "Franc Suisse", 1.07));
            DevisesList.Add(new Devise(3, "Yen", 120));

        }
    }
}
